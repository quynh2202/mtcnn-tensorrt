
==========================
network: native_frozen_Pnet_2.pb,	 batchsize 1, steps 100
  fps 	median: 464.7, 	mean: 465.2, 	uncertainty: 5.7, 	jitter: 39.8
  latency 	median: 0.00215, 	mean: 0.00218, 	99th_p: 0.00325, 	99th_uncertainty: 0.00041

==========================
network: tftrt_fp32_frozen_Pnet_2.pb,	 batchsize 1, steps 100
  fps 	median: 463.0, 	mean: 472.2, 	uncertainty: 7.9, 	jitter: 34.6
  latency 	median: 0.00216, 	mean: 0.00219, 	99th_p: 0.00407, 	99th_uncertainty: 0.00150

==========================
network: tftrt_fp16_frozen_Pnet_2.pb,	 batchsize 1, steps 100
  fps 	median: 509.4, 	mean: 512.6, 	uncertainty: 9.0, 	jitter: 66.3
  latency 	median: 0.00196, 	mean: 0.00202, 	99th_p: 0.00363, 	99th_uncertainty: 0.00123

==========================
network: native_frozen_Pnet_2.pb,	 batchsize 1, steps 100
  fps 	median: 448.7, 	mean: 452.4, 	uncertainty: 4.8, 	jitter: 45.2
  latency 	median: 0.00223, 	mean: 0.00224, 	99th_p: 0.00293, 	99th_uncertainty: 0.00002

==========================
network: tftrt_fp32_frozen_Pnet_2.pb,	 batchsize 1, steps 100
  fps 	median: 454.6, 	mean: 457.3, 	uncertainty: 7.0, 	jitter: 43.3
  latency 	median: 0.00220, 	mean: 0.00230, 	99th_p: 0.00407, 	99th_uncertainty: 0.00317

==========================
network: tftrt_fp16_frozen_Pnet_2.pb,	 batchsize 1, steps 100
  fps 	median: 638.0, 	mean: 621.3, 	uncertainty: 9.6, 	jitter: 88.8
  latency 	median: 0.00157, 	mean: 0.00166, 	99th_p: 0.00251, 	99th_uncertainty: 0.00121
