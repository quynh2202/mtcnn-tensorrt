import tensorrt as trt
import pycuda.driver as cuda
import pycuda.autoinit
import numpy as np
import cv2
import time
TRT_LOGGER = trt.Logger(trt.Logger.WARNING)

def predict_pnet(h_input,context):
    h_input =np.transpose(h_input,[2,0,1])
    batch_size = 1
    shape_out1 = (108, 194, 2)
    shape_out2 = (108, 194, 4)
    h_input = np.array([h_input])
    h_input = h_input.astype(np.float32)
    h1_output = cuda.pagelocked_empty(shape_out1, dtype=np.float32)
    h2_output = cuda.pagelocked_empty(shape_out2, dtype=np.float32)
    # h3_output = cuda.pagelocked_empty(batch_size * 10, dtype=np.float32)

    d_input = cuda.mem_alloc(h_input.nbytes)
    d1_output = cuda.mem_alloc(h1_output.nbytes)
    d2_output = cuda.mem_alloc(h2_output.nbytes)
    # Create a stream in which to copy inputs/outputs and run inference.
    stream = cuda.Stream()
    cuda.memcpy_htod_async(d_input, h_input, stream)
    # Run inference.
    context.execute_async(batch_size=batch_size, bindings=[int(d_input), int(d1_output),
                                                           int(d2_output)],  # ,int(d3_output)],
                          stream_handle=stream.handle)
    # Transfer predictions back from the GPU.
    cuda.memcpy_dtoh_async(h1_output, d1_output, stream)
    cuda.memcpy_dtoh_async(h2_output, d2_output, stream)
    # cuda.memcpy_dtoh_async(h3_output, d3_output, stream)

    d_input.free()
    d1_output.free()
    d2_output.free()
    stream.synchronize()
    return h1_output,h2_output
def predict_rnet(h_input,context,batch_size):
    h_input = np.transpose(h_input,[0,3,1,2])
    #print("shape trong prd",h_input.shape)
    h_input = np.array([h_input])
    h_input = h_input.astype(np.float32)

    shape_out1 = (64, 2)
    shape_out2 = (64, 4)
    h1_output = cuda.pagelocked_empty(shape_out1, dtype=np.float32)
    h2_output = cuda.pagelocked_empty(shape_out2, dtype=np.float32)

    d_input = cuda.mem_alloc(h_input.nbytes)
    d1_output = cuda.mem_alloc(h1_output.nbytes)
    d2_output = cuda.mem_alloc(h2_output.nbytes)
    # Create a stream in which to copy inputs/outputs and run inference.
    stream = cuda.Stream()
    cuda.memcpy_htod_async(d_input, h_input, stream)
    # Run inference.
    context.execute_async(batch_size=batch_size, bindings=[int(d_input), int(d1_output),
                                                           int(d2_output)],  # ,int(d3_output)],
                          stream_handle=stream.handle)
    # Transfer predictions back from the GPU.
    cuda.memcpy_dtoh_async(h1_output, d1_output, stream)
    cuda.memcpy_dtoh_async(h1_output, d1_output, stream)
    cuda.memcpy_dtoh_async(h2_output, d2_output, stream)
    # cuda.memcpy_dtoh_async(h3_output, d3_output, stream)
    d_input.free()
    d1_output.free()
    d2_output.free()
    stream.synchronize()
    return h1_output,h2_output
def predict_onet(h_input,context,batch_size):
    t1 = time.time()
    h_input = np.transpose(h_input,[0,3,1,2])
    #print("shape trong prd",h_input.shape)
    h_input = np.array([h_input])
    h_input = h_input.astype(np.float32)

    shape_out1 = (16, 2)
    shape_out2 = (16, 4)
    shape_out3 = (16,10)
    h1_output = cuda.pagelocked_empty(shape_out1, dtype=np.float32)
    h2_output = cuda.pagelocked_empty(shape_out2, dtype=np.float32)
    h3_output = cuda.pagelocked_empty(shape_out3, dtype=np.float32)

    d_input = cuda.mem_alloc(h_input.nbytes)
    d1_output = cuda.mem_alloc(h1_output.nbytes)
    d2_output = cuda.mem_alloc(h2_output.nbytes)
    d3_output = cuda.mem_alloc(h3_output.nbytes)
    # Create a stream in which to copy inputs/oytesutputs and run inference.
    stream = cuda.Stream()
    cuda.memcpy_htod_async(d_input, h_input, stream)
    # Run inference.
    context.execute_async(batch_size=batch_size, bindings=[int(d_input), int(d1_output),
                                                           int(d2_output),int(d3_output)],
                          stream_handle=stream.handle)
    # Transfer predictions back from the GPU.
    cuda.memcpy_dtoh_async(h1_output, d1_output, stream)
    cuda.memcpy_dtoh_async(h2_output, d2_output, stream)
    cuda.memcpy_dtoh_async(h3_output, d3_output, stream)
    # cuda.memcpy_dtoh_async(h3_output, d3_output, stream)
    d_input.free()
    d1_output.free()
    d2_output.free()
    d3_output.free()
    stream.synchronize()
    return h1_output,h2_output,h3_output
def process_data_rnet(databatch,context_rnet):
    scores = []
    batch_size = 64
    minibatch = []
    cur = 0
    # num of all_data
    n = databatch.shape[0]
    while cur < n:
        # split mini-batch
        minibatch.append(databatch[cur:min(cur + batch_size, n), :, :, :])
        cur += batch_size
    # every batch prediction result
    cls_prob_list = []
    bbox_pred_list = []

    for idx, data in enumerate(minibatch):
        m = data.shape[0]
        real_size = batch_size
        # the last batch
        if m < batch_size:
            keep_inds = np.arange(m)
            # gap (difference)
            gap = batch_size - m
            while gap >= len(keep_inds):
                gap -= len(keep_inds)
                keep_inds = np.concatenate((keep_inds, keep_inds))
            if gap != 0:
                keep_inds = np.concatenate((keep_inds, keep_inds[:gap]))
            data = data[keep_inds]
            real_size = m
        # cls_prob batch*2
        # bbox_pred batch*4
        data = data.astype(np.float32)
        cls_prob, bbox_pred= predict_rnet(data,context_rnet,batch_size)
            #num_batch * batch_size *2
        cls_prob_list.append(cls_prob[:real_size])
        #num_batch * batch_size *4
        bbox_pred_list.append(bbox_pred[:real_size])

        #num_of_data*2,num_of_data*4,num_of_data*10
    return np.concatenate(cls_prob_list, axis=0), np.concatenate(bbox_pred_list, axis=0)

def process_data_onet(databatch,context_onet):
    scores = []
    batch_size = 16
    minibatch = []
    cur = 0
    # num of all_data
    n = databatch.shape[0]
    while cur < n:
        # split mini-batch
        minibatch.append(databatch[cur:min(cur + batch_size, n), :, :, :])
        cur += batch_size
    # every batch prediction result
    cls_prob_list = []
    bbox_pred_list = []
    landmark_pred_list = []
    for idx, data in enumerate(minibatch):
        m = data.shape[0]
        real_size = batch_size
        # the last batch
        if m < batch_size:
            keep_inds = np.arange(m)
            # gap (difference)
            gap = batch_size - m
            while gap >= len(keep_inds):
                gap -= len(keep_inds)
                keep_inds = np.concatenate((keep_inds, keep_inds))
            if gap != 0:
                keep_inds = np.concatenate((keep_inds, keep_inds[:gap]))
            data = data[keep_inds]
            real_size = m
        # cls_prob batch*2
        # bbox_pred batch*4
        data = data.astype(np.float32)

        cls_prob, bbox_pred,landmark_pred = predict_onet(data,context_onet,batch_size)
            #num_batch * batch_size *2
        cls_prob_list.append(cls_prob[:real_size])
        #num_batch * batch_size *4
        bbox_pred_list.append(bbox_pred[:real_size])
        #num_batch * batch_size*10
        landmark_pred_list.append(landmark_pred[:real_size])
        #num_of_data*2,num_of_data*4,num_of_data*10
    return np.concatenate(cls_prob_list, axis=0), np.concatenate(bbox_pred_list, axis=0), np.concatenate(landmark_pred_list, axis=0)

'''img = cv2.imread("/home/tinh/quynhpt/MTCNN-relu/test/img_1_test/1470993087vietnam.jpg")
img = cv2.resize(img,(1789,838))
img = (img-127.5)/255.0
img = np.transpose(img,[2,0,1])
img = img.astype(np.float32)
img = np.array([img])
with open('/home/tinh/quynhpt/MTCNN-relu/tensorrt/save_engine_scale/Pnet.engine', 'rb') as f, trt.Runtime(TRT_LOGGER) as runtime:
    engine = runtime.deserialize_cuda_engine(f.read())
with engine.create_execution_context() as context:
    for i in range(100):
        cls, reg = predict_pnet(img,context)
    sum_ = 0
    for i in range(100):
	t1 = time.time()
	cls, reg = predict_pnet(img,context)
	sum_ +=time.time()-t1
    print("time___",sum_)
'''
