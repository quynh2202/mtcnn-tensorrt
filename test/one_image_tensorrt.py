#coding:utf-8
import sys
sys.path.append('..')
from Detection.MtcnnDetector_tensorrt_time import MtcnnDetector
from Detection.detector import Detector
from Detection.fcn_detector import FcnDetector
from train_models.mtcnn_my_model import P_Net, R_Net, O_Net
from train_models.loader import TestLoader
import cv2
import os
import numpy as np
import time
import tensorrt as trt
TRT_LOGGER = trt.Logger(trt.Logger.WARNING)
test_mode = "ONet"
thresh = [0.6, 0.7, 0.7]
min_face_size = 20
stride = 2
slide_window = False
shuffle = False
detectors = [None, None, None]
prefix = ['../data/MTCNN_model/PNet_landmark/PNet', '../data/MTCNN_model/RNet_landmark/RNet', '../data/MTCNN_model/ONet_landmark/ONet']
epoch = [10, 10, 10]
# epoch = [30,18,22]

batch_size = [2048, 64, 16]
model_path = ['%s-%s' % (x, y) for x, y in zip(prefix, epoch)]
# load pnet model
if slide_window:
    PNet = Detector(P_Net, 12, batch_size[0], model_path[0])
else:
    PNet = FcnDetector(P_Net, model_path[0])
detectors[0] = PNet

# load rnet model
if test_mode in ["RNet", "ONet"]:
    RNet = Detector(R_Net, 24, batch_size[1], model_path[1])
    detectors[1] = RNet

# load onet model
if test_mode == "ONet":
    ONet = Detector(O_Net, 48, batch_size[2], model_path[2])
    detectors[2] = ONet

mtcnn_detector = MtcnnDetector(detectors=detectors, min_face_size=min_face_size,
                               stride=stride, threshold=thresh, slide_window=slide_window)
gt_imdb = []
#gt_imdb.append("35_Basketball_Basketball_35_515.jpg")
#imdb_ = dict()"
#imdb_['image'] = im_path
#imdb_['label'] = 5
# path = "../../DATA/test/lfpw_testImage"]
path = "img_1_test"
for item in os.listdir(path):
    gt_imdb.append(os.path.join(path,item))
test_data = TestLoader(gt_imdb)

with open('../tensorrt/save_engine_scale/Pnet.engine', 'rb') as f, trt.Runtime(TRT_LOGGER) as runtime:
    engine_pnet = runtime.deserialize_cuda_engine(f.read())
with open('../tensorrt/save_engine_10/Rnet.engine', 'rb') as f, trt.Runtime(TRT_LOGGER) as runtime:
    engine_rnet = runtime.deserialize_cuda_engine(f.read())
with open('../tensorrt/save_engine_10/Onet.engine', 'rb') as f, trt.Runtime(TRT_LOGGER) as runtime:
    engine_onet = runtime.deserialize_cuda_engine(f.read())

with engine_pnet.create_execution_context() as context_pnet,engine_rnet.create_execution_context() as context_rnet,engine_onet.create_execution_context() as context_onet:

    t1 = time.time()
    all_boxes,landmarks = mtcnn_detector.detect_face(test_data,context_pnet,context_rnet,context_onet)
# all_boxes, landmarks = mtcnn_detector.detect_single_image(test_data)
    t2 = time.time()
    print("______________________-time",t2-t1)
#assert False
# print("------------------len_box",len(all_boxes))
count = 0
#gt_imdb  =  ["img_1_test/1.jpg"]
for imagepath in gt_imdb:
    print(imagepath)
    image = cv2.imread(imagepath)

    for bbox in all_boxes[count]:

        cv2.putText(image,str(np.round(bbox[4],2)),(int(bbox[0]),int(bbox[1])),cv2.FONT_HERSHEY_TRIPLEX,1,color=(255,0,255))
        # if ( int(bbox[0] > 1160 or int(bbox[1])>1160 or int(bbox[2])>1160 or int(bbox[3]) >1160 ):

        #     print("co > nha")
        cv2.rectangle(image, (int(bbox[0]),int(bbox[1])),(int(bbox[2]),int(bbox[3])),(0,0,255))
        if ((bbox[0]>1160) or(bbox[1]>1160) or (bbox[2]>1160) or(bbox[3]>1160) ) :
            print("___________---co nha __________--")
    '''
        for landmark in landmarks[count]:

        for i in range(len(landmark)//2):
            cv2.circle(image, (int(landmark[2*i]),int(int(landmark[2*i+1]))), 3, (0,0,255))
    '''


    count = count + 1
    #cv2.imwrite("result_landmark/%d.png" %(count),image)
    cv2.imshow("lala",image)
    cv2.waitKey(0)





'''
for data in test_data:
    print type(data)
    for bbox in all_boxes[0]:
        print bbox
        print (int(bbox[0]),int(bbox[1]))
        cv2.rectangle(data, (int(bbox[0]),int(bbox[1])),(int(bbox[2]),int(bbox[3])),(0,0,255))
    #print data
    cv2.imshow("lala",data)
    cv2.waitKey(0)
'''
