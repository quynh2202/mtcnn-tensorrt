
==========================
network: native_frozen_Pnet_2.pb,	 batchsize 1024, steps 100
  fps 	median: 278018.8, 	mean: 280879.3, 	uncertainty: 3379.3, 	jitter: 27328.1
  latency 	median: 0.00368, 	mean: 0.00370, 	99th_p: 0.00510, 	99th_uncertainty: 0.00132

==========================
network: tftrt_fp32_frozen_Pnet_2.pb,	 batchsize 1024, steps 100
  fps 	median: 263228.5, 	mean: 265623.2, 	uncertainty: 1916.6, 	jitter: 16464.8
  latency 	median: 0.00389, 	mean: 0.00388, 	99th_p: 0.00496, 	99th_uncertainty: 0.00030

==========================
network: tftrt_fp16_frozen_Pnet_2.pb,	 batchsize 1024, steps 100
  fps 	median: 260901.9, 	mean: 264315.6, 	uncertainty: 2147.7, 	jitter: 14847.1
  latency 	median: 0.00392, 	mean: 0.00390, 	99th_p: 0.00548, 	99th_uncertainty: 0.00133
