import cv2
import time
import numpy as np
import sys

sys.path.append("../")
from train_models.MTCNN_config import config
from Detection_test_time.nms import py_nms
# import Detection.predict as trt
#import matplotlib.pyplot as plt
#from skimage import data
#from skimage.transform import pyramid_gaussian
import math
import Detection_test_time.predict_tensorrt as trt 
class MtcnnDetector(object):

    def __init__(self,
                 detectors,
                 min_face_size=20,
                 stride=2,
                 threshold=[0.6, 0.7, 0.7],
                 scale_factor=0.79,
                 # scale_factor=0.709,#change
                 slide_window=False):

        self.pnet_detector = detectors[0]
        self.rnet_detector = detectors[1]
        self.onet_detector = detectors[2]
        self.min_face_size = min_face_size
        self.stride = stride
        self.thresh = threshold
        self.scale_factor = scale_factor
        self.slide_window = slide_window

    def convert_to_square(self, bbox):
        """
            convert bbox to square
        Parameters:
        ----------
            bbox: numpy array , shape n x 5
                input bbox
        Returns:
        -------
            square bbox
        """
        square_bbox = bbox.copy()

        h = bbox[:, 3] - bbox[:, 1] + 1
        w = bbox[:, 2] - bbox[:, 0] + 1
        max_side = np.maximum(h, w)
        square_bbox[:, 0] = bbox[:, 0] + w * 0.5 - max_side * 0.5
        square_bbox[:, 1] = bbox[:, 1] + h * 0.5 - max_side * 0.5
        square_bbox[:, 2] = square_bbox[:, 0] + max_side - 1
        square_bbox[:, 3] = square_bbox[:, 1] + max_side - 1
        return square_bbox

    def calibrate_box(self, bbox, reg):
        """
            calibrate bboxes
        Parameters:
        ----------
            bbox: numpy array, shape n x 5
                input bboxes
            reg:  numpy array, shape n x 4
                bboxes adjustment
        Returns:
        -------
            bboxes after refinement
        """

        bbox_c = bbox.copy()
        w = bbox[:, 2] - bbox[:, 0] + 1
        w = np.expand_dims(w, 1)
        h = bbox[:, 3] - bbox[:, 1] + 1
        h = np.expand_dims(h, 1)
        reg_m = np.hstack([w, h, w, h])
        aug = reg_m * reg
        bbox_c[:, 0:4] = bbox_c[:, 0:4] + aug
        return bbox_c


    # def convert_toado_chuan(self,t_index,matrix_are,list_x_y,scale_h,scale_w):
    #     scale_basic=self.scale_factor
    #     ceil_size= 12
    #     list_index_scale= matrix_are[t_index[0],t_index[1]]
    #     list_scale= scale_basic**list_index_scale

    #     list_x_y= np.array(list_x_y)
    #     l_real = list_x_y[list_index_scale]   # toa do thuc tung vung scale
    #     l_real =np.transpose(l_real,[1,0])
    #     list_scale *=scale_h
    #     result_index= (np.array(t_index)-l_real )/ list_scale

    #     result_index_high = result_index + ceil_size/list_scale
    #     return result_index[1] , result_index[0], result_index_high[1], result_index_high[0],list_index_scale
    def convert_toado_chuan(self,t_index,matrix_are,list_x_y,scale_h,scale_w):
        scale_basic=self.scale_factor
        ceil_size= 12
        list_index_scale= matrix_are[t_index[0],t_index[1]]
        # print("list index scale ", list_index_scale.shape)
        list_scale= scale_basic**list_index_scale
        # print("list_sclae sau",list_scale.shape)

        list_x_y= np.array(list_x_y)
        l_real = list_x_y[list_index_scale]   # toa do thuc tung vung scale
        
        l_real =np.transpose(l_real,[1,0])
       
        list_scale_h =np.array(list_scale* scale_h)
        list_scale_w = np.array(list_scale* scale_w)
        
        result_index_0= (t_index[0]-l_real[0] )/ list_scale_h
        result_index_1 = (t_index[1] - l_real[1]) / list_scale_w


        result_index_high_0 = result_index_0 + ceil_size/list_scale_h
        result_index_high_1 = result_index_1 + ceil_size / list_scale_w
       
        return result_index_1 , result_index_0, result_index_high_1, result_index_high_0,list_index_scale

    def reduce_box(self,all_boxes,list_index_scale):
        '''giam so luong box trong tung vung'''
        all_boxes = np.vstack(all_boxes)

        bbox_result = []
        for i in range(np.max(list_index_scale)):
            index = np.where(list_index_scale==i)
            index = index[0]
            bboxes_i = all_boxes[index,:]
            keep = py_nms(bboxes_i[:, 0:5], 0.5, 'Union')
            bbox_result.append(bboxes_i[keep])

        if(len(bbox_result)==0):
            return np.array([])
        return np.vstack(bbox_result)


    def generate_bbox(self,cls_map,reg,matrix_are,list_x_y,threshold,img,scale_h,scale_w):

        stride = 2
        cellsize = 12
        t_index = np.where(cls_map>threshold)
        # print("toa do ngoai",np.array(t_index).shape)
        scale = 1.0
        if (t_index[0].size == 0):
            return np.array([])

        # gia tri cua reg tai toa do t_index
        dx1, dy1, dx2, dy2 = [reg[t_index[0], t_index[1], i] for i in range(4)]
        reg = np.array([dx1,dy1,dx2,dy2])

        score = cls_map[t_index[0],t_index[1]]

        t_index= [t_index[0],t_index[1]]
        x1 , y1,x2,y2,list_index_scale = self.convert_toado_chuan(np.array(t_index,dtype=int)*stride,matrix_are,list_x_y,scale_h,scale_w)
        boundingbox = np.vstack([np.round(x1),
                                 np.round(y1),
                                 np.round(x2),
                                 np.round(y2),
                                 score,
                                 reg])
        boundingbox = boundingbox.T
        boundingbox = self.reduce_box(boundingbox,list_index_scale)
        return boundingbox



    def pad(self, bboxes, w, h):
        """
            pad the the bboxes, alse restrict the size of it
        Parameters:
        ----------
            bboxes: numpy array, n x 5
                input bboxes
            w: float number
                width of the input image
            h: float number
                height of the input image
        Returns :
        ------
            dy, dx : numpy array, n x 1
                start point of the bbox in target image
            edy, edx : numpy array, n x 1
                end point of the bbox in target image
            y, x : numpy array, n x 1
                start point of the bbox in original image
            ex, ex : numpy array, n x 1
                end point of the bbox in original image
            tmph, tmpw: numpy array, n x 1
                height and width of the bbox
        """
        tmpw, tmph = bboxes[:, 2] - bboxes[:, 0] + 1, bboxes[:, 3] - bboxes[:, 1] + 1
        num_box = bboxes.shape[0]

        dx, dy = np.zeros((num_box,)), np.zeros((num_box,))
        edx, edy = tmpw.copy() - 1, tmph.copy() - 1

        x, y, ex, ey = bboxes[:, 0], bboxes[:, 1], bboxes[:, 2], bboxes[:, 3]

        tmp_index = np.where(ex > w - 1)
        edx[tmp_index] = tmpw[tmp_index] + w - 2 - ex[tmp_index]
        ex[tmp_index] = w - 1

        tmp_index = np.where(ey > h - 1)
        edy[tmp_index] = tmph[tmp_index] + h - 2 - ey[tmp_index]
        ey[tmp_index] = h - 1

        tmp_index = np.where(x < 0)
        dx[tmp_index] = 0 - x[tmp_index]
        x[tmp_index] = 0

        tmp_index = np.where(y < 0)
        dy[tmp_index] = 0 - y[tmp_index]
        y[tmp_index] = 0

        return_list = [dy, edy, dx, edx, y, ey, x, ex, tmpw, tmph]
        return_list = [item.astype(np.int32) for item in return_list]

        return return_list

    def detect_pnet(self, im,context):
        """Get face candidates through pnet

        Parameters:
        ----------
        im: numpy array
            input image array

        Returns:
        -------
        boxes: numpy array
            detected boxes before calibration
        boxes_c: numpy array
            boxes after calibration
        """
        t = time.time()
        image_compose,matrix_are,list_x_y,scale_h,scale_w = self.pyramid_norm(im)
        #cls_cls_map1, reg1 = self.pnet_detector.predict(image_compose)
        # print("time pyramid",time.time()-t)
        
        '''image_compose = np.transpose(image_compose,[2,0,1])
        image_compose = image_compose.astype(np.float32)
        image_compose = np.array([image_compose])
        t1 = time.time()
        cls_cls_map, reg = trt.predict_pnet(image_compose,context)'''
	t1 = time.time()
	cls_cls_map, reg = self.pnet_detector.predict(image_compose)
        #print(cls_cls_map-cls_cls_map1)
        #print(reg - reg1)
        #assert False
        t2 = time.time()
        '''for i in range(100):
            cls_cls_map, reg = trt.predict_pnet(image_compose,context)
        #cls_cls_map1, reg1 = self.pnet_detector.predict(image_compose)
        sum_=0
        for i in range(100):
            t1 = time.time()
                cls_cls_map, reg = trt.predict_pnet(image_compose,context)
            sum_ +=time.time()-t1
        print("time pnet------------- tensorrt",sum_)'''
        all_boxes = self.generate_bbox(cls_cls_map[:,:,1],reg,matrix_are,list_x_y,self.thresh[0],image_compose,scale_h,scale_w)
        #print("allbox____________PPPPPPP-",all_boxes.shape)
        if len(all_boxes) == 0:
            return None, None, None, t2-t1
        # merge the detection from first stage
        keep = py_nms(all_boxes[:, 0:5], 0.7, 'Union')

        all_boxes = all_boxes[keep]
        boxes = all_boxes[:, :5]

        bbw = all_boxes[:, 2] - all_boxes[:, 0] + 1
        bbh = all_boxes[:, 3] - all_boxes[:, 1] + 1

        boxes_c = np.vstack([(all_boxes[:, 0] + all_boxes[:, 5] * bbw),
                             (all_boxes[:, 1] + all_boxes[:, 6] * bbh),
                             (all_boxes[:, 2] + all_boxes[:, 7] * bbw),
                             (all_boxes[:, 3] + all_boxes[:, 8] * bbh),
                             all_boxes[:, 4]])
        boxes_c = boxes_c.T
        return boxes, boxes_c, None, t2-t1

    def detect_rnet(self, im, dets,context):
        """Get face candidates using rnet

        Parameters:
        ----------
        im: numpy array
            input image array
        dets: numpy array
            detection results of pnet

        Returns:
        -------
        boxes: numpy array
            detected boxes before calibration
        boxes_c: numpy array
            boxes after calibration
        """
        h, w, c = im.shape
        dets = self.convert_to_square(dets)
        dets[:, 0:4] = np.round(dets[:, 0:4])

        [dy, edy, dx, edx, y, ey, x, ex, tmpw, tmph] = self.pad(dets, w, h)
        num_boxes = dets.shape[0]
        cropped_ims = np.zeros((num_boxes, 24, 24, 3), dtype=np.float32)

        for i in range(num_boxes):
            # print(tmph[i], tmpw[i])
	    if (tmph[i] <0 or tmpw[i]<0 or edy[i]<0 or edx[i]<0):
		continue
            tmp = np.zeros((tmph[i], tmpw[i], 3), dtype=np.uint8)
            tmp[dy[i]:edy[i] + 1, dx[i]:edx[i] + 1, :] = im[y[i]:ey[i] + 1, x[i]:ex[i] + 1, :]
            cropped_ims[i, :, :, :] = (cv2.resize(tmp, (24, 24)) - 127.5) / 128
        # cls_scores : num_data*2
        # reg: num_data*4
        # landmark: num_data*10
#	print("rnet shape",cropped_ims.shape)
	#assert False
        t1 = time.time()
        cls_scores, reg = trt.process_data_rnet(cropped_ims,context)
        #cls_scores , reg ,_= self.rnet_detector.predict(cropped_ims)
        #print(cls_scores1 - cls_scores)
        #print(reg1 - reg)
        t2 = time.time()
        cls_scores = cls_scores[:, 1]
        keep_inds = np.where(cls_scores > self.thresh[1])[0]
        if len(keep_inds) > 0:
            boxes = dets[keep_inds]
            boxes[:, 4] = cls_scores[keep_inds]
            reg = reg[keep_inds]
            # landmark = landmark[keep_inds]
        else:
            return None, None, None, t2 - t1


        keep = py_nms(boxes,0.6)
        boxes = boxes[keep]
        boxes_c = self.calibrate_box(boxes, reg[keep])
        return boxes, boxes_c, None , t2-t1

    def detect_onet(self, im, dets,context):
        """Get face candidates using onet

        Parameters:
        ----------
        im: numpy array
            input image array
        dets: numpy array
            detection results of rnet

        Returns:
        -------
        boxes: numpy array
            detected boxes before calibration
        boxes_c: numpy array
            boxes after calibration
        """
        h, w, c = im.shape
        dets = self.convert_to_square(dets)
        dets[:, 0:4] = np.round(dets[:, 0:4])
        [dy, edy, dx, edx, y, ey, x, ex, tmpw, tmph] = self.pad(dets, w, h)
        num_boxes = dets.shape[0]
        cropped_ims = np.zeros((num_boxes, 48, 48, 3), dtype=np.float32)
        for i in range(num_boxes):
	    if (tmph[i] <0 or tmpw[i] <0 or edy[i]<0 or edx[i]<0):
		continue 
            tmp = np.zeros((tmph[i], tmpw[i], 3), dtype=np.uint8)
            tmp[dy[i]:edy[i] + 1, dx[i]:edx[i] + 1, :] = im[y[i]:ey[i] + 1, x[i]:ex[i] + 1, :]
            cropped_ims[i, :, :, :] = (cv2.resize(tmp, (48, 48)) - 127.5) / 128
        t1 = time.time()
        cls_scores, reg, landmark = trt.process_data_onet(cropped_ims,context)
        #cls_scores1, reg1, landmark1 = self.onet_detector.predict(cropped_ims)
        #print(cls_scores - cls_scores1)
        #print(reg - reg1)
        #assert False
        t2 = time.time()
        cls_scores = cls_scores[:, 1]
        keep_inds = np.where(cls_scores > self.thresh[2])[0]
        if len(keep_inds) > 0:
            # pickout filtered box
            boxes = dets[keep_inds]
            boxes[:, 4] = cls_scores[keep_inds]
            reg = reg[keep_inds]
            landmark = landmark[keep_inds]
        else:
            return None, None, None, t2 - t1

        # width
        w = boxes[:, 2] - boxes[:, 0] + 1
        # height
        h = boxes[:, 3] - boxes[:, 1] + 1
        landmark[:, 0::2] = (np.tile(w, (5, 1)) * landmark[:, 0::2].T + np.tile(boxes[:, 0], (5, 1)) - 1).T
        landmark[:, 1::2] = (np.tile(h, (5, 1)) * landmark[:, 1::2].T + np.tile(boxes[:, 1], (5, 1)) - 1).T
        boxes_c = self.calibrate_box(boxes, reg)

        boxes = boxes[py_nms(boxes, 0.6, "Minimum")]
        keep = py_nms(boxes_c, 0.6, "Minimum")
        boxes_c = boxes_c[keep]
        landmark = landmark[keep]
        return boxes, boxes_c, landmark, t2 - t1

    # use for video
    def detect(self, img,context_pnet,context_rnet,context_onet):
        """Detect face over image
        """
        boxes = None
        t = time.time()

        # pnet
        t1 = 0
        if self.pnet_detector:
            boxes, boxes_c,_= self.detect_pnet(img,context_pnet)
            if boxes_c is None:
                return np.array([]), np.array([])

            t1 = time.time() - t
            t = time.time()
        print("box pnet",boxes_c.shape)
        # rnet
        t2 = 0
        if self.rnet_detector:
            boxes, boxes_c, _ = self.detect_rnet(img, boxes_c,context_rnet)
            if boxes_c is None:
                return np.array([]), np.array([])

            t2 = time.time() - t
            t = time.time()
        print("boxes rnet",boxes_c.shape)
        # onet
        t3 = 0
        if self.onet_detector:
            boxes, boxes_c, landmark = self.detect_onet(img, boxes_c,context_onet)
            if boxes_c is None:
                return np.array([]), np.array([])

            t3 = time.time() - t
            t = time.time()
            # print(
            #    "time cost " + '{:.3f}'.format(t1 + t2 + t3) + '  pnet {:.3f}  rnet {:.3f}  onet {:.3f}'.format(t1, t2,
            #                                                                                                  t3))
        print("boxes_onet",boxes_c.shape)
        return boxes_c, landmark

    def detect_face(self, test_data,context_pnet, context_rnet,context_onet):
        all_boxes = []  # save each image's bboxes
        landmarks = []
        batch_idx = 0

        sum_time = 0
        t1_sum = 0
        t2_sum = 0
        t3_sum = 0
        sum_timeP = 0
        sum_timeR = 0
        sum_timeO = 0
        num_of_img = test_data.size
        empty_array = np.array([])
        # test_data is iter_
        s_time = time.time()
        for databatch in test_data:
            # databatch(image returned)
            # print(gt[batch_idx])
            batch_idx += 1
            
            if batch_idx % 100 == 0:
                c_time = (time.time() - s_time )/100
                print("%d out of %d images done" % (batch_idx ,test_data.size))
                print('%f seconds for each image' % c_time)
                s_time = time.time()


            im = databatch
            # pnet


            if self.pnet_detector is not None :
                st = time.time()
                # ignore landmark
                # print("shape pnet",im.shape)
                boxes, boxes_c, landmark, timeP = self.detect_pnet(im,context_pnet)
                #print("boxes pnet",boxes_c.shape)
		sum_timeP += timeP
                t1 = time.time() - st
                sum_time += t1
                t1_sum += t1
                if boxes_c is None:
                    print("boxes_c is None...")
                    all_boxes.append(empty_array)
                    # pay attention
                    landmarks.append(empty_array)

                    continue
                #print(all_boxes)

            # rnet

            if self.rnet_detector is not None and boxes_c is not None:
                t = time.time()
                # ignore landmark
                # print("shape rnet",boxes_c.shape)
                boxes, boxes_c, landmark,time_R = self.detect_rnet(im, boxes_c, context_rnet)
                #print("boxes_c rnet",boxes_c.shape)
		sum_timeR += time_R
                t2 = time.time() - t
                sum_time += t2
                t2_sum += t2
                if boxes_c is None:
                    all_boxes.append(empty_array)
                    landmarks.append(empty_array)

                    continue
            # onet

            if self.onet_detector is not None and boxes_c is not None:
                t = time.time()
                # print("shape onet",boxes_c.shape)
                boxes, boxes_c, landmark, time_O = self.detect_onet(im, boxes_c, context_onet)
                #print("boxes Onet",boxes_c.shape)
		sum_timeO += time_O
                t3 = time.time() - t
                sum_time += t3
                t3_sum += t3
                if boxes_c is None:
                    all_boxes.append(empty_array)
                    landmarks.append(empty_array)

                    continue

            all_boxes.append(boxes_c)
            landmark = [1]
            landmarks.append(landmark)
       
        return all_boxes, landmarks, sum_time, t1_sum, t2_sum, t3_sum, sum_timeP, sum_timeR, sum_timeO, batch_idx


    def detect_single_image(self, im):
        all_boxes = []  # save each image's bboxes

        landmarks = []

       # sum_time = 0

        t1 = 0
        if self.pnet_detector:
          #  t = time.time()
            # ignore landmark
            boxes, boxes_c, landmark = self.detect_pnet(im)
           # t1 = time.time() - t
           # sum_time += t1
            if boxes_c is None:
                print("boxes_c is None...")
                all_boxes.append(np.array([]))
                # pay attention
                landmarks.append(np.array([]))


        # rnet

        if boxes_c is None:
            print('boxes_c is None after Pnet')
        t2 = 0
        if self.rnet_detector and not boxes_c is  None:
           # t = time.time()
            # ignore landmark
            boxes, boxes_c, landmark = self.detect_rnet(im, boxes_c)
           # t2 = time.time() - t
           # sum_time += t2
            if boxes_c is None:
                all_boxes.append(np.array([]))
                landmarks.append(np.array([]))


        # onet
        t3 = 0
        if boxes_c is None:
            print('boxes_c is None after Rnet')

        if self.onet_detector and not boxes_c is  None:
          #  t = time.time()
            boxes, boxes_c, landmark = self.detect_onet(im, boxes_c)
         #   t3 = time.time() - t
          #  sum_time += t3
            if boxes_c is None:
                all_boxes.append(np.array([]))
                landmarks.append(np.array([]))


        #print(
         #   "time cost " + '{:.3f}'.format(sum_time) + '  pnet {:.3f}  rnet {:.3f}  onet {:.3f}'.format(t1, t2, t3))

        all_boxes.append(boxes_c)
        landmarks.append(landmark)

        return all_boxes, landmarks
    def processed_image(self, img, scale):
        '''
        rescale/resize the image according to the scale
        :param img: image
        :param scale:
        :return: resized image
        '''
        height, width, channels = img.shape
        new_height = int(height * scale)  # resized new height
        new_width = int(width * scale)  # resized new width
        new_dim = (new_width, new_height)
        img_resized = cv2.resize(img, new_dim, interpolation=cv2.INTER_LINEAR)  # resized image
        # img_resized = cv2.resize(img, new_dim, interpolation=interpolation)
        # don't understand this operation
        img_resized = (img_resized - 127.5) / 128
        return img_resized

    def pyramid_norm(self,img,scale=0.79,height = 450,width=350):
        scale = 1.0
        scale=self.scale_factor
        rows, cols,_ = img.shape
        min_size= 12.0

        current_scale = min_size/self.min_face_size
        image = cv2.resize(img,(width,height),interpolation=cv2.INTER_LINEAR) 
        h_new, w_new,_ = image.shape
        h_scale ,w_scale = current_scale*height / rows, current_scale * width /cols
        
        im_resized = self.processed_image(image, current_scale)
        current_height, current_width, _ = im_resized.shape
        # print("shape image resize___-", current_height, current_width)
        min_size_h_w=min(current_height,current_width)

        max_layer = int(math.log(12.0/min_size_h_w,scale))

        # composite_image = np.zeros((int(current_height*max_layer),int(current_width*max_layer),3),dtype=np.float)
        # matrix_are = np.zeros((int(current_height*max_layer),int(current_width*max_layer)),np.int8)
        
        height_comp = math.ceil((1-scale**max_layer)/(1-scale))
        width_comp = height_comp
        height_comp *= current_height
        width_comp *=  current_width
        composite_image = np.zeros((int(height_comp),int(width_comp),3),dtype=np.float)
        matrix_are = np.zeros((int(height_comp),int(width_comp)),dtype=np.int8)
        n_row,n_col= current_height,current_width
        i_row,i_col = 0,0
        stt =0
        list_x_y= []
        max_height, max_width= 0,0
        while min(current_height,current_width)>min_size:
            n_row,n_col= current_height,current_width
            #print("height,width",n_row,n_col)
            composite_image[i_row:i_row+n_row,i_col:i_col+n_col] = im_resized
            matrix_are[i_row:i_row+n_row,i_col:i_col+n_col] = np.ones((n_row,n_col),np.int8)*stt

            current_scale *=scale
            im_resized = self.processed_image(image,current_scale)
            current_height, current_width,_ =  im_resized.shape
            list_x_y.append([i_row,i_col])
            max_height , max_width =max(max_height,i_row+n_row),max(max_width,i_col+n_col)
            if (stt%2 ==1):
                i_row += n_row-1
            else:
                i_col += n_col-1
            stt +=1

        image_pyramid = composite_image[:max_height,:max_width]
        # print(" max height weight _____-", max_height, max_width)
        matrix_are = matrix_are[:max_height,:max_width]
       
        return image_pyramid,matrix_are,list_x_y,h_scale,w_scale

