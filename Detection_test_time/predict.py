# Copyright 2018 The TensorFlow Authors. All Rights Reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ==============================================================================
"""Methods for running the Official Models with TensorRT.

Please note that all of these methods are in development, and subject to
rapid change.
"""
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import argparse
import imghdr
import json
import os
import sys
import time
import cv2
import numpy as np
import tensorflow as tf
from tensorflow.contrib.saved_model.python.saved_model import reader
import tensorflow.contrib.tensorrt as trt

import Detection.imagenet_preprocessing  # pylint: disable=g-bad-import-order

_GPU_MEM_FRACTION = 0.50
_WARMUP_NUM_LOOPS = 5
_LOG_FILE = "log.txt"
_LABELS_FILE = "labellist.json"
_GRAPH_FILE = "frozen_graph.pb"

size_image = 48

################################################################################
# Prep the image input to the graph.
################################################################################

def get_frozen_graph(graph_file):
  """Read Frozen Graph file from disk."""
  with tf.gfile.FastGFile(graph_file, "rb") as f:
    graph_def = tf.GraphDef()
    graph_def.ParseFromString(f.read())
  return graph_def


def get_tftrt_name(graph_name, precision_string):
  return "tftrt_{}_{}".format(precision_string.lower(), graph_name)

def write_graph_to_file(graph_name, graph_def, output_dir):
  """Write Frozen Graph file to disk."""
  output_path = os.path.join(output_dir, graph_name)
  with tf.gfile.GFile(output_path, "wb") as f:
    f.write(graph_def.SerializeToString())

def get_trt_graph_from_calib(graph_name, calib_graph_def, output_dir):
  """Convert a TensorRT graph used for calibration to an inference graph."""
  trt_graph = trt.calib_graph_to_infer_graph(calib_graph_def)
  write_graph_to_file(graph_name, trt_graph, output_dir)
  return trt_graph


################################################################################
# Run the graph in various precision modes.
################################################################################
def get_gpu_config():
  """Share GPU memory between image preprocessing and inference."""
  gpu_options = tf.GPUOptions(per_process_gpu_memory_fraction=_GPU_MEM_FRACTION)
  return tf.ConfigProto(gpu_options=gpu_options)


def get_iterator(data):
  """Wrap numpy data in a dataset."""
  t1= time.time()

  dataset = tf.data.Dataset.from_tensors(data).repeat()
  t2 = time.time()
  print("get iter", t2-t1)
  return dataset.make_one_shot_iterator()


def time_graph(graph_def, data, input_node, output_node,isOnet):
  """Run and time the inference graph.

  This function sets up the input and outputs for inference, warms up by
  running inference for _WARMUP_NUM_LOOPS, then times inference for num_loops
  loops.

  Args:
    graph_def: GraphDef, the graph to be timed.
    data: ndarray of shape [batch_size, height, width, depth], data to be
      predicted.
    input_node: string, the label of the input node where data will enter the
      graph.
    output_node: string, the names of the output node that will
      be returned during inference.
    num_loops: int, number of batches that should run through for timing.

  Returns:
    A tuple consisting of a list of num_loops inference times, and the
    predictions that were output for the batch.
  """
  
  #tf.logging.info("Starting execution")

  tf.reset_default_graph()
  g = tf.Graph()

  with g.as_default():
    t1 = time.time()
    iterator = get_iterator(data)
    return_tensors = tf.import_graph_def(
        graph_def=graph_def,
        input_map={input_node: iterator.get_next()},
        return_elements=output_node
    )

    # Unwrap the returned output node. For now, we assume we only
    # want the tensor with index `:0`, which is the 0th element of the
    # `.outputs` list.
    output1 = return_tensors[0].outputs[0]
    output2 = return_tensors[1].outputs[0]
    if (isOnet):
      output3 = return_tensors[2].outputs[0]

    with tf.Session(graph=g, config=get_gpu_config()) as sess:

      val1 = sess.run(output1)
      val2 = sess.run(output2)
      if (isOnet):
        val3 = sess.run(output3)
  t2 = time.time()
  print("_____________time graph___________--",t2-t1)

  if (isOnet):

    return val1, val2, val3

  return val1, val2


def time_and_log_graph(graph_name, graph_def,data,input_node,output_node, isOnet):
  if isOnet:
    result_cls, result_bbox, result_landmark = time_graph(
      graph_def, data, input_node, output_node,isOnet)
    return result_cls, result_bbox, result_landmark
  # log_stats(graph_name, log_buffer, timings, flags.batch_size)
  else :
    result_cls, result_bbox = time_graph(
        graph_def, data, input_node, output_node,isOnet)
    return result_cls, result_bbox

def run_trt_graph_for_mode(g_name, graph,data,input_node,output_node,isOnet):

  if (isOnet):
    result_cls,result_bbox,result_landmark = time_and_log_graph(g_name, graph, data,input_node,output_node,isOnet)
    return result_cls, result_bbox, result_landmark
  else:
    result_cls,result_bbox = time_and_log_graph(g_name, graph, data,input_node,output_node,isOnet)
    return result_cls, result_bbox


# def get_frozen_grap(g_name):
#   graph = get_frozen_graph(g_name)
#   return graph

def process_O_R(model_name,graph,databatch):
  scores = []
  if (model_name=="Rnet"):
    batch_size = 64
  else:
    batch_size = 16

  minibatch = []
  cur = 0
  #num of all_data
  n = databatch.shape[0]
  while cur < n:
      #split mini-batch
      minibatch.append(databatch[cur:min(cur + batch_size, n), :, :, :])
      cur += batch_size
  #every batch prediction result
  cls_prob_list = []
  bbox_pred_list = []
  landmark_pred_list = []
  for idx, data in enumerate(minibatch):
      m = data.shape[0]
      real_size = batch_size
      #the last batch
      if m < batch_size:
          keep_inds = np.arange(m)
          #gap (difference)
          gap = batch_size - m
          while gap >= len(keep_inds):
              gap -= len(keep_inds)
              keep_inds = np.concatenate((keep_inds, keep_inds))
          if gap != 0:
              keep_inds = np.concatenate((keep_inds, keep_inds[:gap]))
          data = data[keep_inds]
          real_size = m
      #cls_prob batch*2
      #bbox_pred batch*4
      data = data.astype(np.float32)
      if (model_name=="Onet"):
        cls_prob, bbox_pred,landmark_pred = predict(model_name,graph,data)
      else:
        cls_prob,bbox_pred = predict(model_name,graph,data)
      # cls_prob, bbox_pred,landmark_pred = self.sess.run([self.cls_prob, self.bbox_pred,self.landmark_pred], feed_dict={self.image_op: data})
      #num_batch * batch_size *2
      cls_prob_list.append(cls_prob[:real_size])
      #num_batch * batch_size *4
      bbox_pred_list.append(bbox_pred[:real_size])
      #num_batch * batch_size*10
      if(model_name=="Onet"):
        landmark_pred_list.append(landmark_pred[:real_size])

  if (model_name=="Onet"):
    return np.concatenate(cls_prob_list, axis=0), np.concatenate(bbox_pred_list, axis=0), np.concatenate(landmark_pred_list, axis=0)
  else:
    return np.concatenate(cls_prob_list, axis=0), np.concatenate(bbox_pred_list, axis=0)

def predict(model_name,graph,data):

  isOnet = False
  if model_name=="Pnet":
    input_node = "input_image_second"
    #graph_name = "Detection/save_opt_bs/Pnet/tftrt_fp16_frozen_Pnet_2.pb"
    graph_name="save_frozen_final/frozen_Pnet_2.pb"
    output_node = ["prob1","conv4-2/conv4-2"]

  elif model_name=="Rnet":
    input_node = "input_image"
    graph_name = "Detection/save_opt_bs/Rnet/tftrt_fp32_frozen_Rnet_2.pb"

    output_node = ["prob1","conv5-2/conv5-2"]
  elif model_name == "Onet":
    input_node = "input_image"
    graph_name="Detection/save_opt_bs/Onet/tftrt_fp16_frozen_Onet_2.pb"
    output_node = ["prob1","conv6-2/conv6-2","conv6-3/conv6-3"]
    isOnet = True
  else:
    print("No have model")
    assert False

  if (isOnet==True):
    cls_pred, bbox_prob,landmark_prob = run_trt_graph_for_mode(g_name=graph_name,graph=graph,data=data,input_node=input_node,output_node=output_node, isOnet=isOnet)
    return cls_pred, bbox_prob, landmark_prob

  else:
    cls_pred, bbox_prob = run_trt_graph_for_mode(g_name=graph_name,graph=graph,data=data,input_node=input_node,output_node=output_node, isOnet=isOnet)

    return cls_pred, bbox_prob
# def predict_O(model_name,graph,data):
#   input_node = "input_image"
#   graph_name="Detection/save_opt/Onet/tftrt_fp16_frozen_Onet_2.pb"
#   output_node = ["prob1","conv6-2/conv6-2","conv6-3/conv6-3"]
#   isOnet = True

#   cls_pred, bbox_prob,landmark_prob = run_trt_graph_for_mode(g_name=graph_name,graph=graph,data=data,input_node=input_node,output_node=output_node, isOnet=isOnet)
#   return cls_pred, bbox_prob, landmark_prob
# def predict_R
def read_data():
  #data = cv2.imread("test/img_test/danh-sach-doi-tuyen-viet-nam.jpg")
  data = cv2.imread("/home/tamnv/1.jpg")
  data = cv2.resize(data,(48,48))
  data = data.astype(np.float32)
  data = (data-127.5)/255.0
  data = np.array([data])
  return data
data = read_data()
# # data = None
g_name="Detection/save_opt/Onet/tftrt_fp16_frozen_Onet_2.pb"
graph=get_frozen_graph(g_name)
rs = predict(model_name="Onet",graph=graph,data=data)
print(rs)



