import tensorflow as tf
def conv(name , x, filter_size_h,filter_size_w, out_filters, stride_h=1,stride_w=1, group=1, biased=True, padding="VALID"):
    in_filters = int(x.get_shape()[-1])
    with tf.variable_scope(name) as scope:
        # kernel = tf.get_variable(name, shape, trainable=self.trainable)
        kernel = tf.get_variable('weights', shape=[filter_size_h, filter_size_w,in_filters, out_filters ] )
        # This is the common-case. Convolve the input without any further complications.
        output = tf.nn.conv2d(x,kernel,[1,stride_h,stride_w,1],padding=padding)
        # Add the biases
        if biased:
            biases = tf.get_variable('biases', [out_filters])
            output = tf.nn.bias_add(output, biases)
        if relu:
            # ReLU non-linearity
            output = tf.nn.relu(output, name=scope.name)
    return output
    # with tf.variable_scope(name):
    #     n = filter_size * filter_size * out_filters
    #     filter = tf.get_variable('DW', [filter_size, filter_size, in_filters, out_filters], tf.float32, tf.random_normal_initializer(stddev=0.01))
    #     # print(x.get_shape().as_list())
    #     # print(filter.get_shape().as_list())
    #     return tf.nn.conv2d(x, filter, [1, strides, strides, 1], 'SAME')

def relu(x):
    return tf.nn.relu(x)
# def prelu(inp, name):
#     with tf.variable_scope(name):
#         i = int(inp.get_shape()[-1])
#         alpha = tf.get_variable('alpha', shape=(i,))
#         output = tf.nn.relu(inp) + tf.multiply(alpha, tf.multiply(-1.0,tf.nn.relu(-inp)))
#     return output
def prelu(inp, name):
    with tf.variable_scope(name):
        i = int(inp.get_shape()[-1])
        alpha = tf.get_variable('alpha', shape=(i,))
        output = tf.nn.relu(inp) + tf.multiply(alpha, tf.minimum(0.0,inp))
    return output

def max_pool(x, filter, strides, name=None,padding="SAME"):
    return tf.nn.max_pool(x, ksize=[1, filter, filter, 1], strides=[1, strides, strides,1], padding=padding,name=name)
# @layer
def fc(inp, num_out, name, relu=True):
    with tf.variable_scope(name):
        input_shape = inp.get_shape()
        if input_shape.ndims == 4:
            # The input is spatial. Vectorize it first.
            dim = 1
            for d in input_shape[1:].as_list():
                dim *= int(d)
            feed_in = tf.reshape(inp, [-1, dim])
        else:
            feed_in, dim = (inp, input_shape[-1].value)
        weights = tf.get_variable('weights', shape=[dim, num_out])
        print("weight"+name,weights.get_shape())
        biases = tf.get_variable('biases', [num_out])
        op = tf.nn.relu_layer if relu else tf.nn.xw_plus_b
        fc = op(feed_in, weights, biases, name=name)
        return fc
def softmax(target, axis=3,name=None):
    return tf.nn.softmax(target,axis,name)
    # max_axis = tf.reduce_max(target, axis)#, keepdims=True)
    # target_exp = tf.exp(target-max_axis)
    # normalize = tf.reduce_sum(target_exp, axis)#, keepdims=True)
    # softmax = tf.div(target_exp, normalize, name)
    # return softmax
def NetworkP(x):
    x = conv('conv1', x, 3, 3, 10, 1,1)
    print("conv1", x.get_shape())
    x = relu(x)
    x = max_pool(x, 2, 2)
    print("max1",x.get_shape())
    # print(x.get_shape().as_list())
    x = conv('conv2', x, 3, 3, 16, 1,1)
    print("conv2", x.get_shape())
    x = relu(x)
    #x = max_pool(x, 2, 2)
    x = conv('conv3', x, 3, 3, 32, 1,1)
    print("con3", x.get_shape())
    x_in = relu(x)
    #x = max_pool(x, 2, 2)
    x = conv('conv4-1', x, 1, 1, 2, 1,1)
    print("cnv4-1",x.get_shape())
    x_cls = softmax(x,3,name="prob1")
    print("sm",x_cls.get_shape())
    #x = max_pool(x, 2, 2)
    x_bbox = conv('conv4-2', x_in, 1,1,4,1,1)
    print(x.get_shape())
    x_landmark = conv("conv4-3", x_in, 1,1,10,1,1)
    # assert False
    #x = max_pool(x, 2, 2)
    return x_cls, x_bbox, x_landmark

def NetworkR(x):
    x = conv("conv1",x,3,3,28,1,1)
    print(x.get_shape())
    x = relu(x)
    x = max_pool(x,3,2,name="pool1")
    print(x.get_shape())
    x = conv("conv2",x,3,3,48,1,1)
    print(x.get_shape())
    x = relu(x)
    x = max_pool(x,3,2,name="max_pool2",padding="VALID")
    print(x.get_shape())
    x = conv("conv3",x,2,2,64,1,1)
    print(x.get_shape())
    x = relu(x)
    x = fc(x,128,relu=False,name="conv4")
    print(x.get_shape())
    x_in = relu(x)
    x = fc(x_in,2,relu=False,name="conv5-1")
    print(x.get_shape())
    x_cls = softmax(x,1,name="prob1")
    print(x.get_shape())
    x_bbox = fc(x_in,4, name="conv5-2")
    x_landmark = fc(x_in,10,name="conv5-3")
    print(x.get_shape())
    return x_cls,x_bbox, x_landmark

def NetworkO(x):
    x = conv("conv1",x,3,3,32,1,1)
    x = relu(x)
    x = max_pool(x,3,2,name="poo1")
    x = conv("conv2",x,3,3,64,1,1)
    x = relu(x)
    x= max_pool(x,3,2,name="max_pool2", padding="VALID")
    x = conv("conv3",x,3,3,64,1,1)
    x = relu(x)
    x = max_pool(x,2,2,name="pool3")
    x = conv("conv4",x,2,2,128,1,1)
    x = relu(x)
    x = fc(x,256,name="conv5")
    x_in = relu(x)

    x = fc(x_in,2,name="conv6-1")
    x_cls = softmax(x,1,name="prob1")
    x_bbox = fc(x_in,4,name="conv6-2")
    x_landmark = fc(x_in,10,name="conv6-3")
    return x_cls, x_bbox, x_landmark

