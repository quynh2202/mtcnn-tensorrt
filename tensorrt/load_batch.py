import tensorrt as trt
import pycuda.driver as cuda
import pycuda.autoinit
import numpy as np
import cv2
import time
TRT_LOGGER = trt.Logger(trt.Logger.WARNING)
batch_size =1
num_iter = 100
# load file engine
with open('./save_engine_true/Pnet.engine', 'rb') as f, trt.Runtime(TRT_LOGGER) as runtime:
    engine = runtime.deserialize_cuda_engine(f.read())
#print('engine',engine)
#assert False
# cap bo nho
#h_input = cuda.pagelocked_empty(engine.get_binding_shape(0).volume(), dtype=np.float32)
#h_output = cuda.pagelocked_empty(engine.get_binding_shape(1).volume(), dtype=np.float32)
# Allocate device memory for inputs and outputs.
img = cv2.imread("1.jpg")
img = cv2.resize(img,(12,12))
img = (img-127.5)/255.0
img = np.transpose(img,[2,0,1])
img = img.astype(np.float32)
img2 = cv2.imread("1.jpg")
img2 = cv2.resize(img2,(12,12))
img_test = np.array([img,img,img2])
#img = img - img
# img = cv2.resize(img,(48,48))
#print(img.shape)
#img = img - img

#h_input = img_test.astype(np.float32)
#h_input=  np.tile(img,[batch_size,1,1,1])
#img = np.tile(img,[batch_size,1,1,1])
img=  np.array([img])
print(img.shape)
h_input = img.astype(np.float32)
print("shape 0",engine.get_binding_shape(0))
print("shape 1",engine.get_binding_shape(1))
print("shape 2",engine.get_binding_shape(2))
print("shape 3",engine.get_binding_shape(3))
#h_input = cuda.pagelocked_empty(engine.get_binding_shape(0), dtype=np.float32)
#h1_output = cuda.pagelocked_empty(engine.get_binding_shape(1), dtype=np.float32)
arr_time=[]
for i in range(num_iter):
    
    t1 = time.time()
    h1_output = cuda.pagelocked_empty(batch_size*2, dtype=np.float32)
    #h1_output_h = cuda.pagelocked_empty((2,2,1,1), dtype=np.float32)
    h2_output = cuda.pagelocked_empty(batch_size*4, dtype=np.float32)
    h3_output = cuda.pagelocked_empty(batch_size*10, dtype=np.float32)
    #h2_output = cuda.pagelocked_empty(engine.get_binding_shape(2), dtype=np.float32)
    #h3_output = cuda.pagelocked_empty(engine.get_binding_shape(3), dtype=np.float32)
    # assert False
    # h1_output = np.empty(2, dtype = np.float32)
    # h2_output = np.empty(batch_size*4, dtype = np.float32)
    # h3_output = np.empty(batch_size*15, dtype = np.float32)
    d_input = cuda.mem_alloc(h_input.nbytes)
    d1_output = cuda.mem_alloc(h1_output.nbytes)
    d2_output = cuda.mem_alloc(h2_output.nbytes)
    d3_output = cuda.mem_alloc(h3_output.nbytes)
    # Create a stream in which to copy inputs/outputs and run inference.
    stream = cuda.Stream()
    #print("buoc", i)

    #  chay tren cuda
    #print(engine.create_execution_context())
    dis_time = time.time()-t1
    with engine.create_execution_context() as context:
        # Transfer input data to the GPU.
        cuda.memcpy_htod_async(d_input, h_input, stream)
        t1 = time.time()  
        # Run inference.
        '''context.enqueue(batch_size,bindings=[int(d_input), int(d1_output),
            int(d2_output),int(d3_output)],
                stream_handle=stream.handle)''' 
        context.execute_async(batch_size=batch_size,bindings=[int(d_input), int(d1_output),
            int(d2_output)],#,int(d3_output)],
                stream_handle=stream.handle)
        # Transfer predictions back from the GPU.
        cuda.memcpy_dtoh_async(h1_output, d1_output, stream)
        cuda.memcpy_dtoh_async(h2_output, d2_output, stream)
        #cuda.memcpy_dtoh_async(h3_output, d3_output, stream)
        stream.synchronize()
        dis_time += time.time()-t1
        #print("time___-",dis_time)
        #cuda.cudaEventSynchronize() 
        # print(h3_output)
         # Synchronize the stream
    #h1_output.free()

    #print(h1_output)
    #print(h2_output)
    t1=time.time()
    d_input.free()
    d1_output.free()
    d2_output.free()
    d3_output.free()
    stream.synchronize()
    dis_time+=time.time()-t1
    #t2= time.time()
    #dis_time = t2-t1
    #print("dis",dis_time)
    arr_time.append(dis_time)
avg_time = np.mean(arr_time)
print("tb",avg_time)


h1_output = np.reshape(h1_output,(batch_size,2,1,1))
print("cls",h1_output)
print("bbox",h2_output)
#print("landmark",h3_output)

# print(t2-t1)
    # Return the host output.
# return h_output
