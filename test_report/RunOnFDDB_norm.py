#coding:utf-8
import sys


sys.path.append("..")
import argparse
from train_models.mtcnn_model import P_Net, R_Net, O_Net
#from train_models.mtcnn_my_model import  R_Net, O_Net
from train_models.loader import TestLoader
from Detection_test_time.detector import Detector
from Detection_test_time.fcn_detector import FcnDetector
# from Detection_test_time.MtcnnDetector import MtcnnDetector
from Detection_test_time.MtcnnDetector_norm import MtcnnDetector
import cv2
import os
import tensorrt as trt
# data_dir = '../../DATA/'
# out_dir = '../../DATA/FDDB_OUTPUT'
data_det = "./"
data_dir = "./"
out_dir_save = "save_report_acc/"

out_file_predict = "save_report_acc/norm.txt"
file_log = "log_norm.txt"
f_predict = open(out_file_predict,"w")


def get_imdb_fddb(data_dir):
    imdb = []
    nfold = 10
    for n in range(nfold):
        file_name = 'FDDB-folds/FDDB-fold-%02d.txt' % (n + 1)
        file_name = os.path.join(data_dir, file_name)
        fid = open(file_name, 'r')
        image_names = []
        for im_name in fid.readlines():
            image_names.append(im_name.strip('\n'))      
        imdb.append(image_names)
    return imdb        



if __name__ == "__main__":
    test_model = "ONet"
    TRT_LOGGER = trt.Logger(trt.Logger.WARNING)
    thresh = [0.6,0.7,0.7]
    min_face_size = 20
    stride = 2
    slide_window = False
    shuffle = False
    vis = False
    detectors = [None, None, None]
    # # prefix = ['../data/MTCNN_model/PNet/PNet', '../data/MTCNN_model/RNet_pretrain/RNet', '../data/MTCNN_model/ONet_pretrain/ONet']
    prefix = ['../data/MTCNN_model_quynhpt/PNet_muon/PNet', '../data/MTCNN_model_cp/RNet_landmark/RNet', '../data/MTCNN_model_cp/ONet_landmark/ONet']
    epoch = [18, 14, 16]
    batch_size = [2048, 256, 16]
    model_path = ['%s-%s' % (x, y) for x, y in zip(prefix, epoch)]
    # # load pnet model
    if slide_window:
        PNet = Detector(P_Net, 12, batch_size[0], model_path[0])
    else:
        PNet = FcnDetector(P_Net, model_path[0])
    detectors[0] = PNet
    
    # # load rnet model
    if test_model in ["RNet", "ONet"]:
        RNet = Detector(R_Net, 24, batch_size[1], model_path[1])
        detectors[1] = RNet
    
    # # load onet model
    if test_model == "ONet":
        ONet = Detector(O_Net, 48, batch_size[2], model_path[2])
        detectors[2] = ONet
    '''if test_model == "PNet":
        detectors=[PNet,None,None]
    elif test_model == "RNet":
        detectors = [PNet, "RNet",None]
    elif test_model == "ONet":
        detectors = [PNet,"RNet","ONet"]'''
    mtcnn_detector = MtcnnDetector(detectors=detectors, min_face_size=min_face_size,
                                   stride=stride, threshold=thresh, slide_window=slide_window)
    
    '''with open('../tensorrt_report/save_engine/Pnet.engine', 'rb') as f, trt.Runtime(TRT_LOGGER) as runtime:
        engine_pnet = runtime.deserialize_cuda_engine(f.read())
    with open('../tensorrt_report/save_engine/Rnet.engine', 'rb') as f, trt.Runtime(TRT_LOGGER) as runtime:
        engine_rnet = runtime.deserialize_cuda_engine(f.read())
    with open('../tensorrt_report/save_engine/Onet.engine', 'rb') as f, trt.Runtime(TRT_LOGGER) as runtime:
        engine_onet = runtime.deserialize_cuda_engine(f.read())

    with engine_pnet.create_execution_context() as context_pnet,engine_rnet.create_execution_context() as context_rnet,engine_onet.create_execution_context() as context_onet:'''
    if True:
    
        imdb = get_imdb_fddb(data_dir)
        nfold = len(imdb)
        sum_time = 0
        sum_t1_time = 0
        sum_t2_time = 0
        sum_t3_time = 0
        sum_time_pnet = 0
        sum_time_rnet = 0
        sum_time_onet = 0
        num_of_img = 0

        for i in range(nfold):
            image_names = imdb[i]
            #print(image_names)
            dets_file_name = os.path.join(data_det,'FDDB-folds', 'FDDB-det-fold-%02d-out.txt' % (i + 1))
            fid = open(dets_file_name,'w')
            sys.stdout.write('%s ' % (i + 1))
            image_names_abs = [os.path.join(data_dir,'data',image_name+'.jpg') for image_name in image_names]
            test_data = TestLoader(image_names_abs)
            all_boxes,_, s_time, t1_time, t2_time, t3_time, time_pnet, time_rnet, time_onet, batch_id = mtcnn_detector.detect_face(test_data)
            num_of_img += batch_id
            sum_time += s_time
            sum_t1_time += t1_time
            sum_t2_time += t2_time
            sum_t3_time += t3_time
            sum_time_pnet += time_pnet
            sum_time_rnet += time_rnet
            sum_time_onet += time_onet 
            for idx,im_name in enumerate(image_names):
                img_path = os.path.join(data_dir,'data',im_name+'.jpg')
                # print("path file",img_path)
                # assert False

                image = cv2.imread(img_path)
                boxes = all_boxes[idx]
                if boxes is None:
                    fid.write(im_name+'\n')
                    fid.write(str(1) + '\n')
                    fid.write('%f %f %f %f %f\n' % (0, 0, 0, 0, 0.99))
                    continue
                fid.write(im_name+'\n')
                fid.write(str(len(boxes)) + '\n')
                f_predict.write(im_name+'\n')
                f_predict.write(str(len(boxes)) +"\n")            
                for box in boxes:

                    fid.write('%f %f %f %f %f\n' % (float(box[0]), float(box[1]), float(box[2]-box[0]+1), float(box[3]-box[1]+1),box[4]))                
                    f_predict.write('%f %f %f %f %f\n' % (float(box[0]), float(box[1]), float(box[2]-box[0]+1), float(box[3]-box[1]+1),box[4]))
            fid.close()
            
            
        msg = ("time cost in average" +
        '{:.3f}'.format(sum_time/num_of_img) +
        '  pnet {:.3f}  rnet {:.3f}  onet {:.3f}'.format(sum_t1_time/num_of_img, sum_t2_time/num_of_img,sum_t3_time/num_of_img)+
        '  pnet_thuan {:.3f}  rnet_thuan {:.3f}  onet_thuan {:.3f}'.format(sum_time_pnet/num_of_img, sum_time_rnet/num_of_img,sum_time_onet/num_of_img))
        save_log_dir = "log_report"
        log_buffer = open(os.path.join(save_log_dir, file_log), "a")
        log_buffer.write(msg)

        f_predict.close()
