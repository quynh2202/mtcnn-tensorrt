import tensorrt as trt
import uff
TRT_LOGGER= trt.Logger(trt.Logger.WARNING)

# model_file=uff.from_tensorflow_frozen_model("../save_frozen_scale/frozen_Pnet.pb",["prob1","conv4-2/conv4-2"])
model_file=uff.from_tensorflow_frozen_model("./save_frozen/frozen_Onet_10.pb",["prob1","conv6-2/conv6-2","conv6-3/conv6-3"])
# model_file=uff.from_tensorflow_frozen_model("./save_frozen/frozen_Rnet_10.pb",["prob1","conv5-2/conv5-2"])
max_batch_size =16
with trt.Builder(TRT_LOGGER) as builder, builder.create_network() as network, trt.UffParser() as parser:

    parser.register_input("input_image",(3,48, 48))
    parser.register_output("prob1")
    
    parser.register_output("conv6-2/conv6-2")
    
    parser.register_output("conv6-3/conv6-3")
    parser.parse_buffer(model_file, network)
    builder.max_batch_size = max_batch_size
    print(builder.platform_has_fast_int8)
   # assert False
    builder.fp16_mode = True
    builder.max_workspace_size = 2 <<  10 # This determines the amount of memory available to the builder when building an optimized engine and should generally be set as high as possible.
    print("done optimizer")
    with builder.build_cuda_engine(network) as engine:
        #serialized_engine = engine.serialize()
        
        with open('./save_engine/Onet.engine', 'wb') as f:
            f.write(engine.serialize())
print("done")

# with trt.Builder(TRT_LOGGER) as builder:
#     with builder.build_cuda_engine(network) as engine:
#         serialized_engine = engine.serialize()
#         with open('sample.engine', 'wb') as f:
#             f.write(engine.serialize())



# with trt.Builder(TRT_LOGGER) as builder, builder.create_network() as network, trt.OnnxParser(network, TRT_LOGGER) as parser:
#     with open(model_path, 'rb') as model:
#         parser.parse(model.read())
#         builder.max_batch_size = max_batch_size
#         builder.platform_has_fast_int8 = True
#         builder.int8_mode = True
#         builder.max_workspace_size = 1 <<  20 # This determines the amount of memory available to the builder when building an optimized engine and should generally be set as high as possible.
# with trt.Builder(TRT_LOGGER) as builder:
#     with builder.build_cuda_engine(network) as engine:
#         serialized_engine = engine.serialize()
# with trt.Runtime(TRT_LOGGER) as runtime:
#     engine = runtime.deserialize_cuda_engine(serialized_engine)
#     with open('sample.engine', 'wb') as f:
#         f.write(engine.serialize())
